use bincode::{deserialize_from, serialize_into};
use memedb_core::{read_tags, write_tags};
use rayon::prelude::*;
use serde::{Deserialize, Serialize};
use std::{
    collections::{HashMap, HashSet},
    fs::read_dir,
    io::{Read, Write},
    path::{Path, PathBuf},
};

type Error = Box<dyn std::error::Error>;
type Tags = HashSet<String>;

#[derive(Deserialize, Serialize)]
pub struct Client {
    files: HashMap<PathBuf, Tags>,
}

impl Client {
    pub fn new<T: AsRef<Path>>(directory: T) -> Result<Client, Error> {
        let mut client = Client {
            files: HashMap::new(),
        };
        client.update(directory)?;
        Ok(client)
    }

    pub fn empty() -> Client {
        Client {
            files: HashMap::new(),
        }
    }

    pub fn from_cache<T: Read>(cache: T) -> Result<Client, Error> {
        Ok(deserialize_from(cache)?)
    }

    pub fn to_cache<T: Write>(&self, cache: T) -> Result<(), Error> {
        serialize_into(cache, &self)?;
        Ok(())
    }

    pub fn all_tags(&self) -> HashSet<&String> {
        self.files.iter().map(|(_, t)| t).flatten().collect()
    }

    pub fn add_to_file(&mut self, path: &Path, tags: Tags) -> Result<(), Error> {
        if let Some(file) = self.files.get_mut(path.into()) {
            file.extend(tags.clone());
            write_tags(&path, &tags)?;
        }
        Ok(())
    }

    pub fn remove_from_file(&mut self, path: &Path, tags: Tags) -> Result<(), Error> {
        if let Some(file) = self.files.get_mut(path.into()) {
            file.retain(|f| !tags.contains(f));
            write_tags(&path, &tags)?;
        }
        Ok(())
    }

    pub fn filter(&mut self, tags: &Tags) -> Vec<&PathBuf> {
        self.files
            .iter()
            .filter_map(|(f, t)| if t.is_superset(tags) { Some(f) } else { None })
            .collect()
    }

    pub fn update<T: AsRef<Path>>(&mut self, directory: T) -> Result<(), Error> {
        let entries: Vec<_> = read_dir(directory)?
            .into_iter()
            .map(|e| Ok(e?.path()))
            .collect::<Result<_, Error>>()?;

        // TODO: Find a way to better propagate errors inside parallel iterators
        self.files = entries
            .into_par_iter()
            .map(|path| {
                let tags = read_tags(&path).ok()?;
                Some((path, tags))
            })
            .collect::<Option<_>>()
            .ok_or("Couldn't read all tags")?;

        Ok(())
    }
}
